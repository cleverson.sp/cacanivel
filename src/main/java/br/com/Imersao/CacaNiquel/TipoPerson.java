package br.com.Imersao.CacaNiquel;

public enum TipoPerson {
	BANANA(10), AMORA(20),JABOTICABA(50),SETE(100);
	
	private int pontuacao;
	
	private TipoPerson(int pontuacao) {
		this.pontuacao = pontuacao;
	}

	public int getPontuacao() {
		return pontuacao;
	}
	

}
