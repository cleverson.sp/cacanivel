package br.com.Imersao.CacaNiquel;

import java.util.ArrayList;
import java.util.List;

public class Maquina {

	TipoPerson tipoPerson;
	int pontos = 0;
	List<TipoPerson> sorteados = new ArrayList<TipoPerson>();
	Slot slot = new Slot();

	public List<TipoPerson> Alavanca() {
		Slot slot = new Slot();
		
		for (int i = 0; i <= 2; i++) {
			tipoPerson = slot.MontaSlot(slot.personagens);
			sorteados.add(tipoPerson);
			pontos += tipoPerson.getPontuacao(); 
		}
		return sorteados;
	}
	
	public int getPontos() {
		return pontos;
	}

}
