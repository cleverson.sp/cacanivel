package br.com.Imersao.CacaNiquel;

import java.util.Scanner;

public class Console {
	private static Scanner scanner = new Scanner(System.in);

	public static String InicioJogo() {
		Imprimir("Digite s para puxar a alavanca !!!");
		String opcao = scanner.nextLine();

		if (!opcao.equals("s")) {
			Imprimir("Não quer jogar !!!");
			System.exit(0);
		}
		return opcao;
	}

	public static void Imprimir(Object texto) {
		System.out.println(texto + "\n");
	}

}
