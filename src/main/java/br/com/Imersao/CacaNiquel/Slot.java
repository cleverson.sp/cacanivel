package br.com.Imersao.CacaNiquel;

import static br.com.Imersao.CacaNiquel.TipoPerson.BANANA;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Slot {

	TipoPerson tipoPerson;
	List<TipoPerson> personagens = new ArrayList<TipoPerson>();
	
	public Slot() {
		personagens.add(TipoPerson.AMORA);
		personagens.add(BANANA);
		personagens.add(TipoPerson.JABOTICABA);
		personagens.add(TipoPerson.SETE);
	}
		
	public TipoPerson MontaSlot(List<TipoPerson> person) {
		Random random = new Random();
		return person.get(random.nextInt(person.size()));
	}
}
